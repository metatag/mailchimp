defmodule MailChimp.Lists do
  @resource "/lists/"

  def add_member(list_id, email, opts \\ []) do
    base_params = [email_address: email, status: "subscribed"]

    params =
      opts
      |> Keyword.take([:tags])
      |> Keyword.merge(base_params)
      |> Enum.into(%{})

    with %{status: 200} <- MailChimp.post(@resource <> to_string(list_id) <> "/members", params) do
      :ok
    end
  end
end
