defmodule MailChimp.Lists.Members do
  @doc """
  Adds and removes tags from a subscriber.

  ## Options

  * `:add` - List of tags to add
  * `:remove` - List of tags to remove
  """
  def update_tags(list_id, email_address, opts \\ []) do
    tags_to_add =
      opts
      |> Keyword.get(:add, [])
      |> Enum.map(fn tag -> %{name: tag, status: "active"} end)

    tags_to_remove =
      opts
      |> Keyword.get(:remove, [])
      |> Enum.map(fn tag -> %{name: tag, status: "inactive"} end)

    body = %{
      tags: tags_to_add ++ tags_to_remove
    }

    subscriber_hash = subscriber_hash(email_address)

    with %{status: 200} <- MailChimp.post("/lists/#{list_id}/members/#{subscriber_hash}/tags", body) do
      :ok
    end
  end

  defp subscriber_hash(email_address) do
    :md5
    |> :crypto.hash(String.downcase(email_address))
    |> Base.encode16(case: :lower)
  end
end
