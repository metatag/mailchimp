defmodule MailChimp do
  @moduledoc """
  API interface to MailChimp.
  """
  use Tesla, docs: false

  plug Tesla.Middleware.BaseUrl, "https://#{data_center()}.api.mailchimp.com/3.0"
  plug Tesla.Middleware.Headers, [{"Authorization", "Basic " <> authorization()}]
  plug Tesla.Middleware.JSON

  adapter Tesla.Adapter.Hackney


  defp config(key) do
    Application.get_env(:mailchimp, key)
  end

  defp username do
    username = config(:username)
    unless username do
      raise ArgumentError, "Expected :username to be set in :mailchimp config."
    end
    username
  end

  defp api_key do
    api_key = config(:api_key)
    unless api_key do
      raise ArgumentError, "Expected :api_key to be set in :mailchimp config."
    end
    api_key
  end

  defp data_center do
    api_key = api_key()
    [_|dc] = String.split(api_key, "-")
    dc
  end
  defp authorization do
    username = username()
    api_key = api_key()
    Base.encode64("#{username}:#{api_key}")
  end
end
