defmodule MailChimp.Mixfile do
  use Mix.Project

  def project do
    [app: :mailchimp,
     version: "0.1.0",
     elixir: "~> 1.4",
     build_embedded: Mix.env == :prod,
     start_permanent: Mix.env == :prod,
     deps: deps()]
  end

  def application do
    [
      extra_applications: [
        :logger,
        :hackney
      ]
    ]
  end

  defp deps do
    [
      {:hackney, "~> 1.14"},
      {:jason, "~> 1.1"},
      {:tesla, "~> 1.2"}
    ]
  end
end
